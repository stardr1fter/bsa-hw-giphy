package com.bsa.giphyhw.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class QueriedGifsData {
    private String query;
    private List<String> gifs;
}

package com.bsa.giphyhw.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GifFromGiphyDto {
    private String type;

    private String id;

    private String url;

    private String slug;

    @JsonProperty("image_url")
    private String imageUrl;
}



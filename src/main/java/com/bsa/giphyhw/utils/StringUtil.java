package com.bsa.giphyhw.utils;

public class StringUtil {
    public static String withoutTrailing(String source, String sequence) {
        int index = source.indexOf(sequence);
        if (index > 0) {
            return source.substring(0, index);
        }
        return source;
    }
}

package com.bsa.giphyhw.storage;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

@Component
public class GifStorageFactory {
    @SneakyThrows
    public GifStorage createOnDisk(String basePath) {
        var canonical = new File(basePath).getCanonicalPath();
        var path = Path.of(canonical);
        return new GifStorageOnDisk(path);
    }
}

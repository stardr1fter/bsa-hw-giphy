package com.bsa.giphyhw.controller;

import com.bsa.giphyhw.data.QueriedGifsData;
import com.bsa.giphyhw.entity.UserHistoryRecord;
import com.bsa.giphyhw.service.GiphyApiClient;
import com.bsa.giphyhw.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class ApiUserController {
    private static Logger logger = LoggerFactory.getLogger(ApiUserController.class);

    private final GiphyApiClient giphyApiClient;
    private final UserService userService;

    @Autowired
    public ApiUserController(GiphyApiClient client, UserService userService) {
        giphyApiClient = client;
        this.userService = userService;
    }

    @GetMapping("/{userId}/all")
    public ResponseEntity<List<QueriedGifsData>> getAllForUser(@PathVariable String userId) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUserGifs(userId));
    }

    @GetMapping("/{userId}/history")
    public ResponseEntity<List<UserHistoryRecord>> getHistory(@PathVariable String userId) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserHistory(userId));
    }

    @DeleteMapping("/{userId}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void clearHistory(@PathVariable String userId) {
        userService.clearUserHistory(userId);
    }

    @GetMapping("/{userId}/search")
    public ResponseEntity<String> searchGif(
            @PathVariable String userId,
            @RequestParam String query,
            @RequestParam(required = false) String force) {

        var gif = userService.searchGif(userId, query, force != null);
        return ResponseEntity.status(HttpStatus.OK).body(gif);
    }

    @PostMapping("/{userId}/generate")
    public ResponseEntity<String> generateGif(
            @PathVariable String userId,
            @RequestParam String query,
            @RequestParam(required = false) String force) {

        var gif = userService.generateGif(userId, query, force != null);
        return ResponseEntity.status(HttpStatus.OK).body(gif);
    }

    @DeleteMapping("/{userId}/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void resetMemoryCache(
            @PathVariable String userId,
            @PathVariable String query) {

        userService.clearMemoryCache(userId, query);
    }

    @DeleteMapping("/{userId}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserData(@PathVariable String userId) {
        userService.deleteUserData(userId);
    }

}

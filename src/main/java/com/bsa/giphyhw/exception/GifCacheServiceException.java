package com.bsa.giphyhw.exception;

public class GifCacheServiceException extends RuntimeException {
    public GifCacheServiceException(String message) {
        super(message);
    }

    public GifCacheServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}

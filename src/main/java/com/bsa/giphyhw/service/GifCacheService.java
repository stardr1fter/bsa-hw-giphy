package com.bsa.giphyhw.service;

import com.bsa.giphyhw.data.QueriedGifsData;
import com.bsa.giphyhw.dto.GifFromGiphyDto;
import com.bsa.giphyhw.exception.GifCacheServiceException;
import com.bsa.giphyhw.exception.GiphyApiException;
import com.bsa.giphyhw.storage.GifStorage;
import com.bsa.giphyhw.storage.GifStorageFactory;
import com.bsa.giphyhw.utils.FileSystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Service
public class GifCacheService {
    private static final String commonCacheDirectory = "src/main/resources/static/cache";

    private static Logger logger = LoggerFactory.getLogger(GifCacheService.class);

    private final GiphyApiClient giphyApi;
    private final HttpClient httpClient;
    private final GifStorage gifStorage;

    @Autowired
    public GifCacheService(GiphyApiClient giphyApi,
                           HttpClient httpClient,
                           GifStorageFactory gifStorageFactory) {
        this.giphyApi = giphyApi;
        this.httpClient = httpClient;
        this.gifStorage = gifStorageFactory.createOnDisk(commonCacheDirectory);
    }

    public List<QueriedGifsData> getAll(String query) {
        throwIfInvalidQuery(query);
        try {
            return gifStorage.getGifsByQuery(query);
        } catch (IOException e) {
            throw new GifCacheServiceException("Error retrieving cached gifs.", e);
        }
    }

    public QueriedGifsData generate(String query) {
        throwIfInvalidQuery(query);
        try {
            GifFromGiphyDto randomGif = giphyApi.getRandomGif(query);
            var uri = URI.create(randomGif.getImageUrl());
            var request = HttpRequest.newBuilder().uri(uri).GET().build();
            var handler = HttpResponse.BodyHandlers.ofInputStream();
            try (var input = httpClient.send(request, handler).body()) {
                gifStorage.saveGif(input, query, randomGif.getId());
            }
            return new QueriedGifsData(query, gifStorage.getGifs(query)); // FIXME return new as first
        } catch (IOException e) {
            throw new GifCacheServiceException("Error during IO operation", e);
        } catch (InterruptedException e) {
            throw new GifCacheServiceException("Network operation interrupted", e);
        } catch (GiphyApiException e) {
            throw new GifCacheServiceException("Error fetching gif from API", e);
        }
    }

    public void clearAll() {
        try {
            gifStorage.clearStorage();
        } catch (IOException e) {
            throw new GifCacheServiceException("Error deleting cached files", e);
        }
    }

    public List<String> getAllGifs() {
        try {
            return gifStorage.getGifs();
        } catch (IOException e) {
            throw new GifCacheServiceException("Error fetching cached gifs.", e);
        }
    }

    private void throwIfInvalidQuery(String query) {
        if (query == null) {
            return;
        }

        if (query.isBlank()) {
            throw new GifCacheServiceException("Query is blank");
        }

        if (!FileSystemUtil.isValidPath(query)) {
            throw new GifCacheServiceException("Supplied query is not a valid path");
        }

        if (FileSystemUtil.containsSeparator(query)) {
            throw new GifCacheServiceException("Supplied query contains separator");
        }
    }
}

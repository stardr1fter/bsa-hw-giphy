package com.bsa.giphyhw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiphyHwApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiphyHwApplication.class, args);
	}

}

package com.bsa.giphyhw.service;

import com.bsa.giphyhw.data.QueriedGifsData;
import com.bsa.giphyhw.entity.UserHistoryRecord;
import com.bsa.giphyhw.exception.EntityNotFoundException;
import com.bsa.giphyhw.exception.GifCacheServiceException;
import com.bsa.giphyhw.exception.UserServiceException;
import com.bsa.giphyhw.storage.GifStorage;
import com.bsa.giphyhw.storage.GifStorageFactory;
import com.bsa.giphyhw.utils.FileSystemUtil;
import com.bsa.giphyhw.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final String commonCacheDirectory = "src/main/resources/static/cache";

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    private final GifStorageFactory gifStorageFactory;
    private final GifCacheService gifCacheService;
    private final GifStorage commonCacheGifStorage;

    @Autowired
    public UserService(GifStorageFactory gifStorageFactory, GifCacheService gifCacheService) {
        this.gifStorageFactory = gifStorageFactory;
        this.commonCacheGifStorage = gifStorageFactory.createOnDisk(commonCacheDirectory);
        this.gifCacheService = gifCacheService;
    }

    public List<QueriedGifsData> getAllUserGifs(String userId) {
        throwIfInvalidUserId(userId);
        var storage = createStorageForUser(userId);
        try {
            return storage.getGifsByQuery(null);
        } catch (IOException e) {
            throw new UserServiceException("Error retrieving user gifs.", e);
        }
    }

    public List<UserHistoryRecord> getUserHistory(String userId) {
        throwIfInvalidUserId(userId);
        return readUserHistoryRecords(userId);
    }

    public void clearUserHistory(String userId) {
        throwIfInvalidUserId(userId);
        var path = Path.of(getPathForUserHistory(userId));
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new UserServiceException("Error clearing user history", e);
        }
    }

    public String searchGif(String userId, String query, Boolean force) {
        throwIfInvalidUserId(userId);
        throwIfInvalidQuery(query);

        // TODO: implement in-memory caching
        // Search in memory cache at first; update in-memory cache each miss
        // When force=true, ignore memory cache

        try {
            var userGifStorage = createStorageForUser(userId);
            var gifs = userGifStorage.getGifs(query);

            if (gifs.isEmpty()) {
                throw new EntityNotFoundException("Gif doesn't exist in common cache storage.");
            }

            var randomIndex = ThreadLocalRandom.current().nextInt(0, gifs.size());
            return gifs.get(randomIndex);
        } catch (IOException e) {
            throw new UserServiceException("Error searching the gif");
        }
    }

    public String generateGif(String userId, String query, Boolean force) {
        throwIfInvalidUserId(userId);
        throwIfInvalidQuery(query);

        try {
            String gif;
            if (force) {
                gif = gifCacheService.generate(query).getGifs().get(0);
            } else {
                var cachedGifs = gifCacheService.getAll(query)
                        .stream()
                        .flatMap(d -> d.getGifs().stream())
                        .collect(Collectors.toList());

                if (cachedGifs.isEmpty()) {
                    gif = gifCacheService.generate(query).getGifs().get(0);
                } else {
                    var randomIndex = ThreadLocalRandom.current().nextInt(0, cachedGifs.size());
                    gif = cachedGifs.get(randomIndex);
                }
            }

            var userGifStorage = createStorageForUser(userId);
            var gifId = StringUtil.withoutTrailing(Path.of(gif).getFileName().toString(), ".gif");
            commonCacheGifStorage.copyToAnotherStorage(gifId, userGifStorage);
            writeUserHistoryRecord(userId, query, gif);
            return gif;
        } catch (IOException e) {
            throw new UserServiceException("Error generating gif for a user");
        } catch (GifCacheServiceException e) {
            throw new UserServiceException("Error retrieving data from cache");
        }
    }

    public void clearMemoryCache(String userId, String query) {
        throwIfInvalidUserId(userId);
        throwIfInvalidQuery(query);
        // TODO: implement in-memory caching
        // delete all cached data in memory for userId
    }

    public void deleteUserData(String userId) {
        throwIfInvalidUserId(userId);
        try {
            createStorageForUser(userId).clearStorage();
            // TODO: implement in-memory caching
            // also clear user in-memory cache
        } catch (IOException e) {
            throw new UserServiceException("Error deleting user data.");
        }
    }

    private void writeUserHistoryRecord(String userId, String query, String gif) {
        var formatter = new SimpleDateFormat("dd-MM-yyyy");

        var record = new UserHistoryRecord(
                formatter.format(new Date()),
                query.replace(',', '_'),
                gif.replace(',', '_'));

        var path = Path.of(getPathForUserHistory(userId));
        var line = "%s,%s,%s\n".formatted(record.getDate(), record.getQuery(), record.getGif());
        try {
            Files.writeString(path, line, StandardOpenOption.APPEND);
        } catch (IOException e) {
            logger.error("Error writign user history record", e);
        }
    }

    private List<UserHistoryRecord> readUserHistoryRecords(String userId) {
        var path = Path.of(getPathForUserHistory(userId));

        if (!Files.exists(path)) {
            return new ArrayList<>();
        }

        try {
            return Files.lines(path)
                    .filter(s -> !s.isBlank())
                    .map(s -> s.split(","))
                    .map(p -> new UserHistoryRecord(p[0], p[1], p[2]))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UserServiceException("Error reading user history");
        }
    }


    private GifStorage createStorageForUser(String userId) {
        var baseDir = getPathForUserDirectory(userId);
        return gifStorageFactory.createOnDisk(baseDir);
    }

    private String getPathForUserHistory(String userId) {
        return getPathForUserDirectory(userId) + "/history.csv";
    }

    private String getPathForUserDirectory(String userId) {
        return "src/main/resources/static/" + userId;
    }

    private void throwIfInvalidQuery(String query) {
        if (query == null) {
            return;
        }

        if (query.isBlank()) {
            throw new UserServiceException("Query is blank");
        }

        if (!FileSystemUtil.isValidPath(query)) {
            throw new UserServiceException("Query is not a valid path");
        }

        if (FileSystemUtil.containsSeparator(query)) {
            throw new UserServiceException("Query contains separator");
        }
    }

    private void throwIfInvalidUserId(String userId) {
        if (userId == null) {
            throw new UserServiceException("User ID is empty");
        }

        if (userId.isBlank()) {
            throw new UserServiceException("USer is blank");
        }

        if (!FileSystemUtil.isValidPath(userId)) {
            throw new UserServiceException("User ID is not a valid path");
        }

        if (FileSystemUtil.containsSeparator(userId)) {
            throw new UserServiceException("User ID contains separator");
        }
    }
}

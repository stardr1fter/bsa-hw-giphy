package com.bsa.giphyhw.exception;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException() {
        super("Entity doesn't exist.");
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}

package com.bsa.giphyhw.exception;

import com.bsa.giphyhw.controller.ApiCacheController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.stream.Collectors;

@ControllerAdvice
public final class Handler extends ResponseEntityExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(ApiCacheController.class);

    @ExceptionHandler(GifCacheServiceException.class)
    public ResponseEntity<String> handleGifCacheServiceException(
            GifCacheServiceException ex,
            HttpServletRequest req) {

        return getStringResponseEntity(req, ex.getMessage());
    }

    @ExceptionHandler(UserServiceException.class)
    public ResponseEntity<String> handleUserServiceException(
            UserServiceException ex,
            HttpServletRequest req) {

        return getStringResponseEntity(req, ex.getMessage());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        String msg = "Error: missing parameter '%s'".formatted(ex.getParameterName());
        logger.error("%s - %s".formatted(request.getDescription(false), msg));
        return ResponseEntity.badRequest().body(msg);
    }

    private static ResponseEntity<String> getStringResponseEntity(HttpServletRequest req, String message) {
        String url = req.getRequestURL().toString();
        String params = req.getParameterMap().entrySet().stream()
                .map(e -> e.getKey() + "=" + Arrays.toString(e.getValue()))
                .collect(Collectors.joining(", "));

        logger.error("%s - %s - Error: %s".formatted(url, params, message));
        return ResponseEntity.internalServerError().body(message);
    }
}
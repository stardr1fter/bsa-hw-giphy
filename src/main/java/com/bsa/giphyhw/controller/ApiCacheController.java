package com.bsa.giphyhw.controller;

import com.bsa.giphyhw.data.QueriedGifsData;
import com.bsa.giphyhw.service.GifCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class ApiCacheController {
    private static Logger logger = LoggerFactory.getLogger(ApiCacheController.class);

    private final GifCacheService cacheService;

    @Autowired
    public ApiCacheController(GifCacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping("/cache")
    public ResponseEntity<List<QueriedGifsData>> getAllGrouped(
            @RequestParam(required = false) String query) {

        logger.info("List all cached gifs grouped (query=%s)".formatted(query));
        var gifsDataList = cacheService.getAll(query);
        return ResponseEntity.status(HttpStatus.OK).body(gifsDataList);
    }

    @PostMapping("/cache/generate")
    public ResponseEntity<QueriedGifsData> generate(@RequestParam String query) {
        logger.info("Generate gif (query=%s)".formatted(query));
        var gifsData = cacheService.generate(query);
        return ResponseEntity.status(HttpStatus.OK).body(gifsData);
    }

    @DeleteMapping("/cache")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void clearAll() {
        logger.info("Delete all cached gifs from disk");
        cacheService.clearAll();
    }

    @GetMapping("/gifs")
    public ResponseEntity<List<String>> getAllWithoutGrouping() {
        logger.info("List all cached gifs");
        var gifs = cacheService.getAllGifs();
        return ResponseEntity.status(HttpStatus.OK).body(gifs);
    }
}

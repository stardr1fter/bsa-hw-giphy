package com.bsa.giphyhw.filters;

import com.bsa.giphyhw.controller.ApiCacheController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@Component
@Order(1)
public class HeaderValidationServletFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(ApiCacheController.class);

    private final static String requiredHeader = "x-bsa-giphy";

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        Enumeration<String> headerNames = httpRequest.getHeaderNames();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();

                if (headerName.equals(requiredHeader)) {
                    chain.doFilter(request, response);
                    return;
                }
            }
        }

        String msg = "Missing required header: " + requiredHeader;
        httpResponse.setStatus(HttpStatus.FORBIDDEN.value());
        logger.error("%s - Error: %s".formatted(httpRequest.getRequestURL(), msg));
        httpResponse.getWriter().write(msg);
    }
}
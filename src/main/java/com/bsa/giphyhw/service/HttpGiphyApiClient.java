package com.bsa.giphyhw.service;

import com.bsa.giphyhw.dto.GifFromGiphyDto;
import com.bsa.giphyhw.exception.GiphyApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class HttpGiphyApiClient implements GiphyApiClient {

    @Value("${api.giphy-url}")
    private String giphyApiUrl;

    @Value("${api.giphy-key}")
    private String giphyApiKey;

    private final HttpClient client;

    @Autowired
    public HttpGiphyApiClient(HttpClient client) { this.client = client; }

    @Override
    public GifFromGiphyDto getRandomGif(String query) throws GiphyApiException {
        try {
            var response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpStatus.OK.value()) {
                throw new GiphyApiException("Bad status code %d".formatted(response.statusCode()));
            }

            // TODO: extract util method
            var mapper = new ObjectMapper();
            var root = mapper.readTree(response.body());
            var data = root.get("data");
            if (data.isEmpty()) {
                throw new GiphyApiException("Unable to find gif for query");
            }

            return mapper.treeToValue(data, GifFromGiphyDto.class);
        } catch (MismatchedInputException e) {
            throw new GiphyApiException("Bad API response", e);
        } catch (JsonProcessingException e) {
            throw new GiphyApiException("Json parsing error", e);
        } catch (IOException | InterruptedException e) {
            throw new GiphyApiException("HTTP request failed.");
        }
    }

    private HttpRequest buildGetRequest(String query) {
        var builder = UriComponentsBuilder
                .fromHttpUrl(giphyApiUrl)
                .queryParam("api_key", giphyApiKey);

        if (query != null) {
            builder.queryParam("tag", query);
        }

        return HttpRequest
                .newBuilder()
                .uri(builder.build().toUri())
                .GET()
                .build();
    }
}

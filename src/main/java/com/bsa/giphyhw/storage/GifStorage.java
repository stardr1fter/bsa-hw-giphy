package com.bsa.giphyhw.storage;


import com.bsa.giphyhw.data.QueriedGifsData;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public interface GifStorage {
    void saveGif(InputStream input, String query, String gifId) throws IOException;

    List<QueriedGifsData> getGifsByQuery(String query) throws IOException;

    List<String> getGifs(String query) throws IOException;

    Optional<String> getRandomGif(String query, List<String> ignoredIdentifiers);

    List<String> getGifs() throws IOException;

    void clearStorage() throws IOException;

    void copyToAnotherStorage(String gifId, GifStorage anotherStorage) throws IOException;
}

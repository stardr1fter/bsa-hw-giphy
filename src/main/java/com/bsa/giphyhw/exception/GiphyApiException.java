package com.bsa.giphyhw.exception;

public class GiphyApiException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Giphy api error.";

    public GiphyApiException() {
        super(DEFAULT_MESSAGE);
    }

    public GiphyApiException(String message) {
        super(message);
    }

    public GiphyApiException(String message, Throwable cause) {
        super(message, cause);
    }
}

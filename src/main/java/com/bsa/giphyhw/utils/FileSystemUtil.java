package com.bsa.giphyhw.utils;

import com.bsa.giphyhw.exception.GifCacheServiceException;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileSystemUtil {
    public static boolean isValidPath(String path) {
        try {
            Paths.get(path);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
        return true;
    }

    public static boolean containsSeparator(String str) {
        return str.matches("[\\\\/]");
    }

    private static String trimToWebRoot(String absolutePath) {
        return trimToWebRoot(absolutePath, "static");
    }

    private static String trimToWebRoot(String absolutePath, String webRootDirName) {

        Path path = Path.of(absolutePath);

        for (int i = 0; i < path.getNameCount(); i++) {
            var part = path.getName(i).toString();
            if (part.equals(webRootDirName)) {
                var fromWebRoot = path.subpath(i + 1, path.getNameCount());
                return fromWebRoot.toString().replace('\\', '/');
            }
        }

        throw new GifCacheServiceException("Unable to locate web root.");
    }
}

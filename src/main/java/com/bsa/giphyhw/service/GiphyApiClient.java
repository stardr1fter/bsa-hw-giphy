package com.bsa.giphyhw.service;

import com.bsa.giphyhw.dto.GifFromGiphyDto;
import com.bsa.giphyhw.exception.GiphyApiException;

public interface GiphyApiClient {
    GifFromGiphyDto getRandomGif(String query) throws GiphyApiException;
}

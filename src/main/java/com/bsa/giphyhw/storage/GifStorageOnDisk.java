package com.bsa.giphyhw.storage;

import com.bsa.giphyhw.data.QueriedGifsData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

public class GifStorageOnDisk implements GifStorage {
    private static Logger logger = LoggerFactory.getLogger(GifStorageOnDisk.class);

    private final Path basePath;

    public GifStorageOnDisk(Path basePath) {
        this.basePath = basePath;
    }

    @Override
    public void saveGif(InputStream input, String query, String gifId) throws IOException {
        Path gifPath = createPathForGif(query, gifId);
        Files.createDirectories(gifPath.getParent());
        Files.copy(input, gifPath, StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public List<QueriedGifsData> getGifsByQuery(String query) {
        var dir = new File(basePath.toString());

        if (!dir.exists()) {
            return new ArrayList<>();
        }

        var files = dir.listFiles();
        Arrays.sort(files, Comparator.comparingLong(File::lastModified).reversed());

        var queriesStream = Arrays.stream(files)
                .filter(File::isDirectory)
                .map(File::toPath)
                .map(Path::getFileName)
                .map(Path::toString);

        if (query != null) {
            queriesStream = queriesStream.filter(q -> q.equals(query));
        }

        return queriesStream
                .map(q -> {
                    ArrayList<String> gifs = null;
                    try {
                        gifs = new ArrayList<>(getGifs(q));
                    } catch (IOException e) {
                        logger.error("Error reading gif file names from disk", e);
                        gifs = new ArrayList<>();
                    }
                    return new QueriedGifsData(q, gifs);
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getGifs(String query) throws IOException {
        Path gifsPath = createPathForGifsDirectory(query);
        var dir = new File(gifsPath.toString());
        if (!dir.exists()) {
            return new ArrayList<>();
        }

        var files = dir.listFiles();
        Arrays.sort(files, Comparator.comparingLong(File::lastModified).reversed());

        return Arrays.stream(files)
                .sorted(Comparator.comparingLong(File::lastModified).reversed())
                .filter(file -> !file.isDirectory())
                .map(File::toPath)
                .map(Path::toString)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<String> getRandomGif(String query, List<String> ignoredIdentifiers) {
        return Optional.empty();
    }

    @Override
    public List<String> getGifs() throws IOException {
        return getGifsByQuery(null)
                .stream()
                .map(QueriedGifsData::getGifs)
                .flatMap(Collection::stream)
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    @Override
    public void clearStorage() throws IOException {
        FileSystemUtils.deleteRecursively(basePath);
    }

    @Override
    public void copyToAnotherStorage(String gifId, GifStorage anotherStorage) throws IOException {
        var gifs = getGifs();
        var pathToSourceGif = gifs.stream().filter(g -> g.endsWith(gifId + ".gif")).findFirst();

        if (pathToSourceGif.isEmpty()) {
            throw new IllegalArgumentException("Attempt to copy a gif that doesn't exist");
        }

        var file = new File(pathToSourceGif.get());
        try (var input = new FileInputStream(file)) {
            var query = Path.of(pathToSourceGif.get()).getParent().getFileName().toString();
            anotherStorage.saveGif(input, query, gifId);
        }
    }

    private Path createPathForGifsDirectory(String query) {
        return Paths.get(basePath.toString(), query);
    }

    private Path createPathForGif(String query, String gifId) {
        return Paths.get(basePath.toString(), query, gifId + ".gif");
    }
}

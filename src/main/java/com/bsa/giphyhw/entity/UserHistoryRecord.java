package com.bsa.giphyhw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserHistoryRecord {
    private String date;
    private String query;
    private String gif;
}
